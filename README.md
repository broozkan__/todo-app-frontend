# Todo App 

- A react application for creating todo list. 
- You can see a hosted version on <a href="http://34.133.161.6" target="_blank">here</a>.

#### Architecture & Development
- This application integrated with a RESTful API provider. Here is <a href="https://gitlab.com/broozkan__/todo-app-frontend.git" target="_blank">backend service</a>. 
During development process TDD procedure adopted with UI test tool `cypress` and CDC. 

## Requirements

###### for build, test
* A .env file should include
    - REACT_APP_API_ENDPOINT (A backend RESTful API service url)

    - REACT_APP_PACT_TOKEN (A pact token for CDC test `pactflow.io`)

* Node 8 or above
* Git

###### for CI/CD
* Gitlab CI/CD environment variables
    - DOCKER_USERNAME (for authenticating to Docker)
    - DOCKER_PASSWORD (for authenticating to Docker)
    - GCLOUD_SERVICE_KEY (for deploying GKE)


## Installation

Clone the repo and install the dependencies.
```bash
git clone https://gitlab.com/broozkan__/todo-app-frontend.git
cd todo-app-frontend
```
```bash
npm install
```

## Testing
This app has UI interaction test and CDC pact as consumer.
These tests also running on gitlab-ci pipeline with `gitlab-ci.yml` file
1: Start server:
```bash
npm start
```
2: Run Add todo test:
```bash
npm run cy:run --spec "cypress/integration/add_todo.spec.js"
```

## Build
###### With Using Docker
You can also run this app as a Docker container:

- Step 1: Clone the repo
```bash
git clone https://gitlab.com/broozkan__/todo-app-frontend.git
cd todo-app-frontend
```
- Step 2: Build the Docker image

```bash
docker build -t todo-app-frontend .
```

- Step 3: Run the Docker container locally:

```bash
docker run -p 8080:8080 -d todo-app-frontend
```

###### With Npm

You can run it on `http://localhost` with:
```bash
npm start
```

### Deployment

###### Deploy on GKE
- For deploying your application to GKE you have to have a service account which should have the right permissions for deployment.
- You can read <a href="https://cloud.google.com/kubernetes-engine/docs/how-to/iam" target="_blank">this</a> this guide for how to do that.

After having the right permissions:
- Step 1: Download the service account key file from Google Cloud

- Step 2: Active service account:
```bash
gcloud auth activate-service-account --key-file /path/to/gcloud-service-key.json
```

- Step 3: Set Google Cloud project id and zone informations:
```bash
gcloud config set project $GKE_PROJECT_ID
gcloud config set compute/zone $GKE_ZONE
```

- Step 4: Get cluster credentials (create a cluster on GKE if it is not exist) :
```bash
gcloud container clusters get-credentials $GKE_CLUSTER_NAME
```

- Step 5: Apply cloud build informations:
```bash
kubectl apply -f simple-app.yaml
```

- Step 6: Restart service for update image (optional):
```bash
kubectl rollout restart deployment/simple-react
```
