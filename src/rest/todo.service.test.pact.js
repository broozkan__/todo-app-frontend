import TodoService from './todo.service';
import * as Pact from '@pact-foundation/pact';
import Todo from '../todo';

describe('TodoService API', () => {

    const todoService = new TodoService('http://localhost', global.port);

    // a matcher for the content type "application/json" in UTF8 charset
    // that ignores the spaces between the ";2 and "charset"
    const contentTypeJsonMatcher = Pact.Matchers.term({
        matcher: "application\\/json; *charset=utf-8",
        generate: "application/json; charset=utf-8"
    });

    describe('getTodo()', () => {

        beforeEach((done) => {
            global.provider.addInteraction({
                state: 'a todo exists',
                uponReceiving: 'a GET request for a todo',
                withRequest: {
                    method: 'GET',
                    path: '/api/todos/42',
                    headers: {
                        'Accept': contentTypeJsonMatcher
                    }
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: Pact.Matchers.somethingLike([new Todo('Superman', 42)])
                }
            }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            todoService.getTodo(42)
                .then(todo => {
                    expect(todo[0].title).toEqual('Superman');
                })
                .then(() => {
                    global.provider.verify()
                        .then(() => done(), error => {
                            done.fail(error)
                        })
                });
        });

    });

    describe('createTodo()', () => {

        beforeEach((done) => {
            global.provider.addInteraction({
                state: 'provider allows todo creation',
                uponReceiving: 'a POST request to create a todo',
                withRequest: {
                    method: 'POST',
                    path: '/api/todos',
                    headers: {
                        'Accept': contentTypeJsonMatcher,
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: new Todo('Superman')
                },
                willRespondWith: {
                    status: 201,
                    headers: {
                        'Content-Type': Pact.Matchers.term({
                            matcher: "application\\/json; *charset=utf-8",
                            generate: "application/json; charset=utf-8"
                        })
                    },
                    body: Pact.Matchers.somethingLike(
                        new Todo('Superman', 42))
                }
            }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            todoService.createTodo(new Todo('Superman'))
                .then(todo => {
                    expect(todo.id).toEqual(42);
                })
                .then(() => {
                    global.provider.verify()
                        .then(() => done(), error => {
                            done.fail(error)
                        })
                });
        });
    });

});