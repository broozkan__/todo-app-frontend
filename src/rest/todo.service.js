import Todo from "../todo";
import adapter from 'axios/lib/adapters/http';
const axios = require('axios');

class TodoService {

    constructor(baseUrl, port) {
        this.baseUrl = baseUrl;
        this.port = port;
    }

    getTodo(todoId) {
        if (todoId == null) {
            throw new Error("todoId must not be null!");
        }
        return axios.request({
            method: 'GET',
            url: `/api/todos/${todoId}`,
            baseURL: `${this.baseUrl}:${this.port}`,
            headers: {
                'Accept': 'application/json; charset=utf-8'
            }
        }, adapter).then((response) => {
            const todo = response.data;
            return new Promise((resolve, reject) => {
                try {
                    console.log(todo);
                    this._validateIncomingTodo(todo[0]);
                    resolve(todo);
                } catch (error) {
                    reject(error);
                }
            });
        });
    };

    createTodo(todo) {
        this._validateTodoForCreation(todo);
        return axios.request({
            method: 'POST',
            url: `/api/todos`,
            baseURL: `${this.baseUrl}:${this.port}`,
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            data: todo
        }, adapter).then((response) => {
            const todo = response.data;
            return new Promise((resolve, reject) => {
                try {
                    this._validateIncomingTodo(todo);
                    resolve(todo);
                } catch (error) {
                    reject(error);
                }
            });
        });
    };

    _validateIncomingTodo(todo) {
        Todo.validateId(todo);
        Todo.validateTitle(todo);
    }

    _validateTodoForCreation(todo) {
        delete todo.id;
        Todo.validateTitle(todo);
    }

}

export default TodoService;