class Todo {
    constructor(title, id) {
        this.id = id;
        this.title = title;
    }



    static validateTitle(todo) {
        if (typeof todo.title !== 'string') {
            throw new Error(`Todo title must be a string! Invalid value: ${todo.title}`);
        }
    }

    static validateId(todo) {
        if (typeof todo.id !== 'number') {
            throw new Error(`Todo id must be a number! Invalid value: ${todo.id}`)
        }
    }
}

export default Todo;