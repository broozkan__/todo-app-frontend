import React, { Component } from 'react';
import api from '../../services/api';


class NewTodo extends Component {

    constructor() {
        super()

        this.state = {
            title: ''
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleOnChange = this.handleOnChange.bind(this)
    }




    handleOnChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })

    }

    async handleOnSubmit(e) {
        e.preventDefault()

        const submitResponse = await api.post('api/todos', this.state)

        if (submitResponse.status != 400) {
            alert("New todo added!")

            this.setState({
                title: ''
            })
            window.location.reload()

        } else {
            alert("Oops! The error occured.")
        }

    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleOnSubmit}>
                    <div className="form-group">
                        <label for="title">Todo Title</label>
                        <input id="title" data-testid="todoTitle" name="title" onChange={this.handleOnChange} value={this.state.title} placeholder="Type todo title..." required />
                    </div>
                    <div className="form-group">
                        <button type="submit" data-testid="saveButton">Save</button>
                    </div>
                </form>
            </div>

        )
    }
}

export default NewTodo