import React, { Component } from 'react';
import api from '../../services/api';


class ListTodo extends Component {

    constructor() {
        super()

        this.state = {
            todos: [],
            is_todos_loaded: false
        }
        this.getTodo = this.getTodo.bind(this)

    }


    componentDidMount() {
        this.getTodo()
    }

    async getTodo() {
        const todos = await api.get('/api/todos')
        console.log(todos);

        this.setState({
            todos: todos.data,
            is_todos_loaded: true
        })
    }

    render() {

        let todosJsx = 'Loading...'
        if (this.state.is_todos_loaded) {
            todosJsx = this.state.todos.map((item) => {
                return (
                    <tr>
                        <td>{item.title}</td>
                    </tr>
                )
            })
        }


        return (
            <div class="table-responsive">
                <table>
                    <thead>
                        <th>Title</th>
                    </thead>
                    <tbody>
                        {todosJsx}
                    </tbody>
                </table>
            </div>

        )
    }
}

export default ListTodo