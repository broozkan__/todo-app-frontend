import React from 'react';
import NewTodo from './components/Form/NewTodo';
import ListTodo from './components/Table/ListTodo';

const App = () => {

  return (
    <>
      <div className="header">
        <h2>Todo App</h2>
      </div>
      <div className="container">
        <div id="main-wrapper">
          <NewTodo />
          <ListTodo />
        </div>
      </div>

    </>
  );
};

export default App;