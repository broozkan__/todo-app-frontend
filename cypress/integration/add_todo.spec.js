describe('Creating a todo', () => {
    it('Displays the todo in the list', () => {
        cy.visit('http://localhost:3000');

        cy.get('[data-testid="todoTitle"]')
            .type('New todo');

        cy.get('[data-testid="saveButton"]')
            .click();

        cy.get('[data-testid="todoTitle"]')
            .should('have.value', '');

        cy.contains('New todo');
    });
});